require 'rails_helper'

RSpec.describe DnsRecordsController, type: :controller do

  context 'create dns_records' do

    let(:sample_payload) {
        { "dns_records" => {
          "ip" => "1.1.1.1",
          "hostnames_attributes" => [
            {
              "hostname" => "lorem.com"
            }
          ]
        }
      }
    }

    it 'creates a dns record and it\'s associated hostnames with the given payload' do

      post :create, body: sample_payload.to_json, format: :json

      expect(response).to have_http_status(:created)
      expect(response.body).to eq ({ id: 1 }.to_json)

      created_dns_record = DnsRecord.find_by_ip_address('1.1.1.1')

      expect(created_dns_record).not_to be nil
      
      created_hostname = Hostname.find_by_hostname('lorem.com')

      expect(created_hostname).not_to be nil

      expect(created_dns_record.hostnames).to include created_hostname
    end

    it 'fails if any of the hostnames already exist for the dns_record' do
      
      dns_record = create(:dns_record, ip_address: '1.1.1.1')
      hostname = create(:hostname)

      dns_record.hostnames << hostname
      
      post :create, body: sample_payload.to_json, format: :json

      expect(response).to have_http_status(:not_acceptable)
    end

  end

  context 'show dns_records' do
    
    let!(:setup) {
      dns_1 = create(:dns_record, ip_address: '1.1.1.1')
      dns_2 = create(:dns_record, ip_address: '2.2.2.2')
      dns_3 = create(:dns_record, ip_address: '3.3.3.3')
      dns_4 = create(:dns_record, ip_address: '4.4.4.4')
      dns_5 = create(:dns_record, ip_address: '5.5.5.5')

      lorem = create(:hostname, hostname: 'lorem.com')
      ipsum = create(:hostname, hostname: 'ipsum.com')
      dolor = create(:hostname, hostname: 'dolor.com')
      amet  = create(:hostname, hostname: 'amet.com')
      sit   = create(:hostname, hostname: 'sit.com')

      dns_1.hostnames.concat [lorem, ipsum, dolor, amet]
      dns_2.hostnames.concat [ipsum]
      dns_3.hostnames.concat [ipsum, dolor, amet]
      dns_4.hostnames.concat [ipsum, dolor, sit, amet]
      dns_5.hostnames.concat [dolor, sit]
    }

    it 'requires a page param' do
      expect { 
        get :index 
      }.to raise_error ActionController::ParameterMissing
    end

    it 'works without a filter' do
      get :index, params: { page: 1 }

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq (
        {
          "total_records" => 5,
          "records" => [
            {
              "id" => 1,
              "ip_address" => "1.1.1.1"
            },
            {
              "id" => 2,
              "ip_address" => "2.2.2.2"
            },
            {
              "id" => 3,
              "ip_address" => "3.3.3.3"
            },
            {
              "id" => 4,
              "ip_address" => "4.4.4.4"
            },
            {
              "id" => 5,
              "ip_address" => "5.5.5.5"
            }
          ],
          "related_hostnames" => [
            {
              "hostname" => "lorem.com",
              "count" => 1
            },
            {
              "hostname" => "sit.com",
              "count" => 2
            },
            {
              "hostname" => "amet.com",
              "count" => 3
            },
            {
              "hostname" => "dolor.com",
              "count" => 4
            },
            {
              "hostname" => "ipsum.com",
              "count" => 4
            }
          ]
        }
      )
    end
    
    it 'shows all created dns records that correspond to the given filters' do

      get :index, params: { page: 1 , included: ['ipsum.com', 'dolor.com'], excluded: ['sit.com']}

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq ({
        "total_records" => 2,
          "records" => [
            {
            "id" => 1,
            "ip_address" => "1.1.1.1"
            },
            {
            "id" => 3,
            "ip_address" => "3.3.3.3"
            }
          ],
          "related_hostnames" => [
            {
            "hostname" => "lorem.com",
            "count" => 1
            },
            {
            "hostname" => 'amet.com',
            "count" => 2
            }
          ]
        })
    end
  end
end
