require 'rails_helper'

RSpec.describe DnsRecord, type: :model do
  it 'validates format of ip address' do
    expect {
      create(:dns_record, ip_address: 'invalid format')
    }.to raise_error ActiveRecord::RecordInvalid
  end
end
