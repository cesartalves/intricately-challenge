class DnsRecordsController < ApplicationController

  def index
    page = params.require(:page)

    included = params[:included].presence || []
    excluded = params[:excluded].presence || []
                                   
    dns_records = DnsRecordsRepository.get_dns_records_with_filters(included: included, excluded: excluded, page: page)
    
    result_records = HostnamesRepository.get_hostnames_from(dns_records)
                                        .except(included.concat(excluded))
   
    render json: {
        total_records: dns_records.pluck(:id).count, 
        records: serialize(dns_records),
        related_hostnames: serialize(result_records)
    }
    
  end

  def create
    payload = JSON.parse request.body.read

    dns_records = payload['dns_records']

    ip_address = dns_records['ip']

    hostnames = dns_records['hostnames_attributes']

    begin

      record = DnsRecordsRepository.create_dns_and_hostnames(ip_address, hostnames)
      render json: { id: record.id }, status: :created

    rescue ActiveRecord::RecordNotUnique
      render status: :not_acceptable, json: ''
    end
  end
end
