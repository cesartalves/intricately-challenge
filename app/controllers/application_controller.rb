class ApplicationController < ActionController::API
  
  protected

  def serialize(serializable_resource)
    ActiveModelSerializers::SerializableResource.new(serializable_resource).as_json
  end
end
