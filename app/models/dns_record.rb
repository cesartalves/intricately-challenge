class DnsRecord < ApplicationRecord

  has_and_belongs_to_many :hostnames
  
  validates_format_of :ip_address, with: /(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/, on: :create

end
