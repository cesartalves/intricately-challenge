class DnsRecordsRepository

  class << self

    def get_dns_records_with_filters(included: [], excluded: [], page: 1)

      dns_records = DnsRecord
                .joins(:hostnames)                 
                .includes(:hostnames)
                .group([:id, :ip_address])
      
      dns_records = dns_records
                      .having('SUM(hostnames.hostname IN (?)) = ?', excluded, 0) if excluded.present?
      dns_records = dns_records
                      .having('SUM(hostnames.hostname IN (?)) = ?', included, included.size) if included.present?
     
      filtered_records = dns_records.page(page)
        
      filtered_records
    end

    def create_dns_and_hostnames(ip_address, hostnames = [])
      DnsRecord.transaction do
        retrieved_dns_record = DnsRecord.find_or_create_by!(ip_address: ip_address)
  
        hostnames.map do |hostname|
  
          retrieved_hostname = Hostname.find_or_create_by!(hostname)
   
          retrieved_dns_record.hostnames << retrieved_hostname
        end 
        
        retrieved_dns_record
      end
    end
  end
end