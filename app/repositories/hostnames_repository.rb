class HostnamesRepository
  
  class << self
    
    def get_hostnames_from(dns_records)

      @result_records = Hostname.joins(:dns_records)
                                .where(dns_records: { id: dns_records.pluck(:id) })
                                .select('hostnames.hostname', 'COUNT(hostnames.hostname) as count')
                                .group('hostnames.hostname')
                                .order('count')
      self
    end
    
    def except(excluded_hostnames)
      result_records.where.not(hostnames: { hostname: excluded_hostnames })
    end

    def result_records
      @result_records ||= Hostname
    end
  end
end