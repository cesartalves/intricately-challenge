class HostnameSerializer < ActiveModel::Serializer
  attributes :hostname, 'count' # count would probably raise if we tried a Hostname.all, but I'm keeping it this way for the sake of the challenge.
end
