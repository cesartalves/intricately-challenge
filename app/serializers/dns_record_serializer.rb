class DnsRecordSerializer < ActiveModel::Serializer
  attributes :id, :ip_address
end
