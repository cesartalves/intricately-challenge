# README

Challenge done for Intricately

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=cesartalves_intricately-challenge&metric=alert_status)](https://sonarcloud.io/dashboard?id=cesartalves_intricately-challenge)

## See it live!

https://intricately-challenge-cesar.herokuapp.com//dns_records?page=1

## Setting up the application

- `docker-compose up -d`
- `docker exec -it intricate_web bash`
- `bundle install`
- `bundle exec rails db:migrate`
- `bundle exec rails db:seed`

## Running the application

- `bundle exec rails s -b 0.0.0.0`

## Running tests

- `bundle exec rails db:migrate RAILS_ENV=test`
- `bundle exec rspec`

## Creating a DnsRecord

**Will create a hostname and return an existing DNS id.**

```
curl -X POST \
  http://localhost:3001/dns_records \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 142' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:3001' \
  -H 'Postman-Token: 3e9c844d-c146-4645-8fbe-9d72aaa3654e,cc7198ca-528e-466d-bea5-74397f4e7aaa' \
  -H 'User-Agent: PostmanRuntime/7.20.1' \
  -H 'cache-control: no-cache' \
  -d '{ "dns_records" : {
    "ip" : "1.1.1.1",
    "hostnames_attributes" : [
        {
          "hostname": "my_hostname.com"
        }
      ]
    }
}'
```

## Listing DnsRecords

**Example request from challenge.**
```
curl -X GET \
  'http://localhost:3001/dns_records?excluded[]=sit.com&included[]=ipsum.com,dolor.com&page=1' \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: localhost:3001' \
  -H 'Postman-Token: 6e5c6e33-1388-495f-80a0-f7d5f69a44f7,85b1b35e-74c2-4d20-9fd7-b0b7384f182f' \
  -H 'User-Agent: PostmanRuntime/7.20.1' \
  -H 'cache-control: no-cache'
```
