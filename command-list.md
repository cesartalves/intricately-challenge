Here are the all the commands run from start to finish of the challenge (aside from git)
This is for self reference and to show the dev process more clearly

docker-compose up -d
docker exec -it intricate_web bash


gem install rails -v 5.4.2
rails new . --database=mysql
rails s -b 0.0.0.0


rails g rspec:install

rails g model DNSRecord ip_address:string:index
rails db:migrate && rails db:migrate RAILS_ENV=test
rails g controller DNSRecords

rails g model Hostname address:string:index
rails db:migrate && rails db:migrate RAILS_ENV=test

rails g migration CreateJoinTableDnsRecordsHostnames dns_records hostnames
rails g migration AddUniqueIndexToDnsRecordsHostnames
rails c --sandbox (to check if unique key raises ActiveREcord::RecordNotUnique)

gem install bundler
bundle update --bundler

rails g migration RenameHostnameColumn

rails generate migration add_index_to_dns_records ip_address:uniq
rails generate migration add_index_to_hostnames hostname:uniq

rails g serializer dns_record
rails g serializer hostname

