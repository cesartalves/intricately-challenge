# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

dns_1 = DnsRecord.create(ip_address: '1.1.1.1')
dns_2 = DnsRecord.create(ip_address: '2.2.2.2')
dns_3 = DnsRecord.create(ip_address: '3.3.3.3')
dns_4 = DnsRecord.create(ip_address: '4.4.4.4')
dns_5 = DnsRecord.create(ip_address: '5.5.5.5')

lorem = Hostname.create(hostname: 'lorem.com')
ipsum = Hostname.create(hostname: 'ipsum.com')
dolor = Hostname.create(hostname: 'dolor.com')
amet  = Hostname.create(hostname: 'amet.com')
sit   = Hostname.create(hostname: 'sit.com')

dns_1.hostnames.concat [lorem, ipsum, dolor, amet]
dns_2.hostnames.concat [ipsum]
dns_3.hostnames.concat [ipsum, dolor, amet]
dns_4.hostnames.concat [ipsum, dolor, sit, amet]
dns_5.hostnames.concat [dolor, sit]