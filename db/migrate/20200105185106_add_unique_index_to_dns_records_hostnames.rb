class AddUniqueIndexToDnsRecordsHostnames < ActiveRecord::Migration[5.2]
  def change
    add_index :dns_records_hostnames, [:hostname_id, :dns_record_id], unique: true
  end
end
