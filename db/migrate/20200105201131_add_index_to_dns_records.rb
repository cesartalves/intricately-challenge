class AddIndexToDnsRecords < ActiveRecord::Migration[5.2]
  def change
    # remove_index :dns_records, :ip_address
    add_index :dns_records, :ip_address, unique: true
  end
end
