class RenameHostnameColumn < ActiveRecord::Migration[5.2]
  def change
    rename_column :hostnames, :address, :hostname
  end
end
