class AddIndexToHostnames < ActiveRecord::Migration[5.2]
  def change
    # remove_index :hostnames, :hostname
    add_index :hostnames, :hostname, unique: true
  end
end
