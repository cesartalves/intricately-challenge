# remember to docker-compose build :-

FROM ruby:2.5.7

RUN apt-get update
RUN apt-get install -y nodejs

RUN addgroup -gid 1000 docker && adduser docker -gid 1000 --uid 1000 --disabled-password --gecos ""
RUN sh -c "echo 'docker ALL=NOPASSWD: ALL' >> /etc/sudoers"

RUN mkdir /var/app
RUN chown -Rf docker:docker /var/app

USER docker

WORKDIR /var/app