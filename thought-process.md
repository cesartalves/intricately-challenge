## Initial Thoughts

### Data modeling

**DNSRecord** can be the name of the first model
ip_address should be an unique string.

I could actually use the ip as a primary_key, for I assume it should be unique, but since the test's asking for the return of an id on 'create', I'll stick to using a unique key.

**Hostname**
address: string. Unique.

Maybe because of the payload format, it'd more convenient for the field to be named 'hostname' 


### Many-to-Many relation

I find that we could use a simple join table; I don't find we need a custom join table with a meaningful name
However I'll need to add_index to garantee a relation is not created twice.

has_and_belongs_to_many 

rails g migration CreateJoinTableDnsRecordsHostnames dns_records hostnames

### Controllers

**create (/dns_records \[POST\])**

Should I restrict creation of hostnames only on new DNSRecords?

I decided to restrict only the creation of duplicate hostnames - that is, I'll use a find_or_create_by_ip_address and after that try to create the hostnames on the payload. If one of them fails, all other will fail, which asks for a transaction in the creation. I can return a 406 :not_acceptable error along with a message in this case.


**index (/dns_records \[GET\])**

DNSRecord.includes(:hostname) -> query databases only twice, rails way.

result format:


### Filters

include: where
exclude: where.not

### Tests

controller and route tests are core

### Design Patterns

From the top of my head I could use repository patterns for the querying, if it gets too long. Policy pattern for defining the per_page of the pagination, since the API spec only specifies the current_page

### Additional things

Restrict creation of dns_record on valid ip address (maybe regex?)

